from tkinter import *
import sys
import requests
import configparser

def main():
    global Fenetre
    global Code_Telecommande
    global Config
    global img_fav1,cmd_fav1
    global Logo1,Commande1
    global Logo2,Commande2
    global Logo3,Commande3
    global Logo4,Commande4
    global Logo5,Commande5
    global Logo6,Commande6
    global Logo7,Commande7
    global Logo8,Commande8
    global Logo9,Commande9

    Config = configparser.ConfigParser()
    Lecture_Ini()
    Fenetre= Tk()
    Fenetre.title("FREEBOX")
    
    img_power=PhotoImage(file="./icons/power.png")
    a1=Button(Fenetre,image=img_power,borderwidth=0,command=cmd_power).grid(row=0, column=1)

    img_home=PhotoImage(file="./icons/home.png")
    a3=Button(Fenetre, image=img_home,borderwidth=0,command=cmd_home).grid(row=0, column=3)
    
    img_yellow=PhotoImage(file="./icons/yellow.png")
    a4=Button(Fenetre, image=img_yellow,borderwidth=0,command=cmd_yellow).grid(row=1, column=1)

    img_haut=PhotoImage(file="./icons/haut.png")
    a5=Button(Fenetre, image=img_haut,borderwidth=0,command=cmd_haut).grid(row=1, column=2)

    img_green=PhotoImage(file="./icons/green.png")
    a6=Button(Fenetre, image=img_green,borderwidth=0,command=cmd_green).grid(row=1, column=3)

    img_gauche=PhotoImage(file="./icons/gauche.png")
    a7=Button(Fenetre, image=img_gauche,borderwidth=0,command=cmd_gauche).grid(row=2, column=1)

    img_ok=PhotoImage(file="./icons/ok.png")
    a8=Button(Fenetre, image=img_ok,borderwidth=0,command=cmd_ok).grid(row=2, column=2)

    img_droite=PhotoImage(file="./icons/droite.png")
    a9=Button(Fenetre, image=img_droite,borderwidth=0,command=cmd_droite).grid(row=2, column=3)

    img_red=PhotoImage(file="./icons/red.png")
    a10=Button(Fenetre, image=img_red,borderwidth=0,command=cmd_red).grid(row=3, column=1)

    img_bas=PhotoImage(file="./icons/bas.png")
    a11=Button(Fenetre, image=img_bas,borderwidth=0,command=cmd_bas).grid(row=3, column=2)

    img_blue=PhotoImage(file="./icons/blue.png")
    a12=Button(Fenetre, image=img_blue,borderwidth=0,command=cmd_blue).grid(row=3, column=3)

    img_vol_plus=PhotoImage(file="./icons/vol_plus.png")
    a13=Button(Fenetre, image=img_vol_plus,borderwidth=0,command=cmd_vol_plus).grid(row=4, column=0)

    img_touche1=PhotoImage(file="./icons/touche1.png")
    a14=Button(Fenetre, image=img_touche1,borderwidth=0,command=cmd_touche1).grid(row=4, column=1)

    img_touche2=PhotoImage(file="./icons/touche2.png")
    a15=Button(Fenetre, image=img_touche2,borderwidth=0,command=cmd_touche2).grid(row=4, column=2)

    img_touche3=PhotoImage(file="./icons/touche3.png")
    a16=Button(Fenetre, image=img_touche3,borderwidth=0,command=cmd_touche3).grid(row=4, column=3)

    img_prgm_plus=PhotoImage(file="./icons/prgm_plus.png")
    a17=Button(Fenetre, image=img_prgm_plus,borderwidth=0,command=cmd_prgm_plus).grid(row=4, column=4)

    img_touche4=PhotoImage(file="./icons/touche4.png")
    a18=Button(Fenetre, image=img_touche4,borderwidth=0,command=cmd_touche4).grid(row=5, column=1)

    img_mute=PhotoImage(file="./icons/mute.png")
    a19=Button(Fenetre, image=img_mute,borderwidth=0,command=cmd_mute).grid(row=5, column=0)

    img_touche4=PhotoImage(file="./icons/touche4.png")
    a20=Button(Fenetre, image=img_touche4,borderwidth=0,command=cmd_touche4).grid(row=5, column=1)

    img_touche5=PhotoImage(file="./icons/touche5.png")
    a21=Button(Fenetre, image=img_touche5,borderwidth=0,command=cmd_touche5).grid(row=5, column=2)

    img_touche6=PhotoImage(file="./icons/touche6.png")
    a22=Button(Fenetre, image=img_touche6,borderwidth=0,command=cmd_touche6).grid(row=5, column=3)

    img_vol_moins=PhotoImage(file="./icons/vol_moins.png")
    a23=Button(Fenetre, image=img_vol_moins,borderwidth=0,command=cmd_vol_moins).grid(row=6, column=0)

    img_touche7=PhotoImage(file="./icons/touche7.png")
    a24=Button(Fenetre, image=img_touche7,borderwidth=0,command=cmd_touche7).grid(row=6, column=1)

    img_touche8=PhotoImage(file="./icons/touche8.png")
    a25=Button(Fenetre, image=img_touche8,borderwidth=0,command=cmd_touche8).grid(row=6, column=2)

    img_touche9=PhotoImage(file="./icons/touche9.png")
    a26=Button(Fenetre, image=img_touche9,borderwidth=0,command=cmd_touche9).grid(row=6, column=3)

    img_prgm_moins=PhotoImage(file="./icons/prgm_moins.png")
    a27=Button(Fenetre, image=img_prgm_moins,borderwidth=0,command=cmd_prgm_moins).grid(row=6, column=4)

    img_touche0=PhotoImage(file="./icons/touche0.png")
    a28=Button(Fenetre, image=img_touche0,borderwidth=0,command=cmd_touche0).grid(row=7, column=2)

    img_piste_precedente=PhotoImage(file="./icons/piste_precedente.png")
    a29=Button(Fenetre, image=img_piste_precedente,borderwidth=0,command=cmd_piste_precedente).grid(row=8, column=0)

    img_precedent=PhotoImage(file="./icons/precedent.png")
    a30=Button(Fenetre, image=img_precedent,borderwidth=0,command=cmd_precedent).grid(row=8, column=1)

    img_play=PhotoImage(file="./icons/play.png")
    a31=Button(Fenetre, image=img_play,borderwidth=0,command=cmd_play).grid(row=8, column=2)

    img_suivant=PhotoImage(file="./icons/suivant.png")
    a32=Button(Fenetre, image=img_suivant,borderwidth=0,command=cmd_suivant).grid(row=8, column=3)

    img_piste_suivante=PhotoImage(file="./icons/piste_suivante.png")
    a33=Button(Fenetre,image=img_piste_suivante,borderwidth=0,command=cmd_piste_suivante).grid(row=8, column=4)

    if Logo1!='':
        img_fav1=PhotoImage(file=Logo1)
        b1=Button(Fenetre,image=img_fav1,borderwidth=0,bg="white",command=cmd_fav1).grid(row=0,column=5)

    if Logo2!='':
        img_fav2=PhotoImage(file=Logo2)
        b2=Button(Fenetre,image=img_fav2,borderwidth=0,bg="white",command=cmd_fav2).grid(row=1,column=5)

    if Logo3!='':
        img_fav3=PhotoImage(file=Logo3)
        b3=Button(Fenetre,image=img_fav3,borderwidth=0,bg="white",command=cmd_fav3).grid(row=2,column=5)

    if Logo4!='':
        img_fav4=PhotoImage(file=Logo4)
        b4=Button(Fenetre,image=img_fav4,borderwidth=0,bg="white",command=cmd_fav4).grid(row=3,column=5)

    if Logo5!='':
        img_fav5=PhotoImage(file=Logo5)
        b5=Button(Fenetre,image=img_fav5,borderwidth=0,bg="white",command=cmd_fav5).grid(row=4,column=5)

    if Logo6!='':
        img_fav6=PhotoImage(file=Logo6)
        b6=Button(Fenetre,image=img_fav6,borderwidth=0,bg="white",command=cmd_fav6).grid(row=5,column=5)

    if Logo7!='':
        img_fav7=PhotoImage(file=Logo7)
        b7=Button(Fenetre,image=img_fav7,borderwidth=0,bg="white",command=cmd_fav7).grid(row=6,column=5)

    if Logo8!='':
        img_fav8=PhotoImage(file=Logo8)
        b8=Button(Fenetre,image=img_fav8,borderwidth=0,bg="white",command=cmd_fav8).grid(row=7,column=5)

    if Logo9!='':
        img_fav9=PhotoImage(file=Logo9)
        b8=Button(Fenetre,image=img_fav9,borderwidth=0,bg="white",command=cmd_fav9).grid(row=8,column=5)

    Fenetre.mainloop()

def cmd_fav1():
    envoi(Commande1)
def cmd_fav2():
    envoi(Commande2)
def cmd_fav3():
    envoi(Commande3)
def cmd_fav4():
    envoi(Commande4)
def cmd_fav5():
    envoi(Commande5)
def cmd_fav6():
    envoi(Commande6)
def cmd_fav7():
    envoi(Commande7)
def cmd_fav8():
    envoi(Commande8)
def cmd_fav9():
    envoi(Commande9)

def cmd_power():
    envoi("power")
def cmd_home():
    envoi("home")
def cmd_ok():
    envoi("ok")
def cmd_touche1():
    envoi("1&long=true")
def cmd_touche2():
    envoi("2&long=true")
def cmd_touche3():
    envoi("3&long=true")
def cmd_touche4():
    envoi("4&long=true")
def cmd_touche5():
    envoi("5&long=true")
def cmd_touche6():
    envoi("6&long=true")
def cmd_touche7():
    envoi("7&long=true")
def cmd_touche8():
    envoi("8&long=true")
def cmd_touche9():
    envoi("9&long=true")
def cmd_touche0():
    envoi("0&long=true")
def cmd_vol_plus():
    envoi("vol_inc")
def cmd_vol_moins():
    envoi("vol_dec")
def cmd_prgm_plus():
    envoi("prgm_inc")
def cmd_prgm_moins():
    envoi("prgm_dec")
def cmd_piste_precedente():
    envoi("prev")
def cmd_precedent():
    envoi("bwd")
def cmd_play():
    envoi("play")
def cmd_suivant():
    envoi("fwd")
def cmd_piste_suivante():
    envoi("next")
def cmd_mute():
    envoi("mute")
def cmd_red():
    envoi("red")
def cmd_green():
    envoi("green")
def cmd_blue():
    envoi("blue")
def cmd_yellow():
    envoi("yellow")
def cmd_haut():
    envoi("up")
def cmd_bas():
    envoi("down")
def cmd_gauche():
    envoi("left")
def cmd_droite():
    envoi("right")

def quitte():
    global Fenetre
    Fenetre.destroy
    sys.exit()

def envoi(bouton):
    global Code_Telecommande
    for i in bouton.split(";"):
        r = requests.get("http://hd1.freebox.fr/pub/remote_control?code=" + Code_Telecommande + "&key=" + i)

def Lecture_Ini():
    global Config
    global Code_Telecommande
    global Logo1,Commande1
    global Logo2,Commande2
    global Logo3,Commande3
    global Logo4,Commande4
    global Logo5,Commande5
    global Logo6,Commande6
    global Logo7,Commande7
    global Logo8,Commande8
    global Logo9,Commande9
    Config.read('telecommande_freebox.ini')
    Code_Telecommande=Config.get('parametres','code_telecommande')
    Logo1 = Config.get('favoris','logo1')
    Commande1 = Config.get('favoris','commande1')
    Logo2 = Config.get('favoris','logo2') 
    Commande2 = Config.get('favoris','commande2')
    Logo3 = Config.get('favoris','logo3')
    Commande3 = Config.get('favoris','commande3')
    Logo4 = Config.get('favoris','logo4')
    Commande4 = Config.get('favoris','commande4')
    Logo5 = Config.get('favoris','logo5')
    Commande5 = Config.get('favoris','commande5')
    Logo6 = Config.get('favoris','logo6')
    Commande6 = Config.get('favoris','commande6')
    Logo7 = Config.get('favoris','logo7')
    Commande7 = Config.get('favoris','commande7')
    Logo8 = Config.get('favoris','logo8')
    Commande8 = Config.get('favoris','commande8')
    Logo9 = Config.get('favoris','logo9')
    Commande9 = Config.get('favoris','commande9')

main()

